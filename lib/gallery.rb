module GalleryHelper
  def galleries
    @items.select { |item| item[:kind] == 'gallery' }
  end

  def sorted_galleries
    galleries.sort_by { |gal| attribute_to_time(gal[:created_at]) }.reverse
  end

  def gallery_cover(item)
    gallery_images(item)[0]
  end

  def gallery_images(item)
    image_root = "images/galleries/#{item[:image_id]}"
    Dir.entries("content/#{image_root}").select do |entry|
      /([^\s]+(\.(?i)(jpg|png|gif|bmp))$)/.match(entry)
    end.map { |entry| "/#{image_root}/#{entry}" }
  end
end

include GalleryHelper
